<?php

namespace App\Controller;

use App\Helper\CategoryHelper;
use App\Helper\ProductHelper;
use App\Helper\ResponseHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    public function index()
    {
        $vuepath = __DIR__.'/../../static/index.html';
        return new Response(file_get_contents($vuepath));
    }

    /**
     * @Route("/categories", methods={"GET"})
     *
     * @param Request $request
     * @param CategoryHelper $helper
     *
     * @return JsonResponse
     */
    public function getCategories(Request $request, CategoryHelper $helper): JsonResponse
    {
        try {
            $categories = $helper->getCategories($request);
        } catch (DBALException | PDOException | ORMException | DriverException | \Exception $e) {
            return ResponseHelper::ko($e->getMessage(), $e->getCode());
        }

        return ResponseHelper::ok($categories);
    }

    /**
     * @Route("/category/{categorySlug}/products", methods={"GET"})
     * @Route("/category/{categorySlug}", methods={"GET"})
     *
     * @param string $categorySlug
     * @param Request $request
     * @param CategoryHelper $helper
     * @param ProductHelper $helper
     *
     * @return JsonResponse
     */
    public function getCategoryProducts(
        string $categorySlug,
        Request $request,
        CategoryHelper $categoryHelper,
        ProductHelper $productHelper
    ): JsonResponse {
        try {
            $category = $categoryHelper->getCategoryBySlug($categorySlug);
            $products = $productHelper->getProducts($category['id'], $request->query->all());
        } catch (DBALException | PDOException | ORMException | DriverException | \Exception $e) {
            return ResponseHelper::ko($e->getMessage(), $e->getCode());
        }

        return ResponseHelper::ok($products);
    }

    /**
     * @Route("/category/{categorySlug}/product/{productSlug}", methods={"GET"})
     * @Route("/product/{productSlug}", methods={"GET"})
     *
     * @param string|null $categorySlug
     * @param string $productSlug
     * @param Request $request
     * @param CategoryHelper $helper
     * @param ProductHelper $helper
     *
     * @return JsonResponse
     */
    public function getProduct(
        string $productSlug,
        Request $request,
        CategoryHelper $categoryHelper,
        ProductHelper $productHelper,
        string $categorySlug = null
    ): JsonResponse {
        try {
            if (!empty($categorySlug)) {
                $category = $categoryHelper->getCategoryBySlug($categorySlug);
            }

            $product = $productHelper->getProductBySlug($productSlug);
        } catch (DBALException | PDOException | ORMException | DriverException | \Exception $e) {
            return ResponseHelper::ko($e->getMessage(), $e->getCode());
        }

        return ResponseHelper::ok($product);
    }
}
