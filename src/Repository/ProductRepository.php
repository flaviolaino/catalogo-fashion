<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[]
     */
    public function findByCateogry(int $categoryId, int $limit = null, int $offset = null)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->andWhere('p.category_id = :id')
            ->setParameter('id', $categoryId)
            ->andWhere('p.public = 1')
            ->orderBy('p.name', 'ASC');

        if (!empty($limit)) {
            $queryBuilder->setMaxResults($limit);
        }

        if (!empty($offset)) {
            $queryBuilder->setFirstResult($offset);
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;
    }

    public function findOneBySlug(string $slug): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('p.public = 1')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
