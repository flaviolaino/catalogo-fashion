<?php

namespace App\Exception;
use Symfony\Component\HttpFoundation\Response;

class NotFoundException extends \Exception
{
    public function __construct($message = "Not Found", $code =  Response::HTTP_NOT_FOUND)
    {
        return parent::__construct($message, $code);
    }
}
