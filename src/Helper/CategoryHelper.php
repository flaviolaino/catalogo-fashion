<?php

namespace App\Helper;

use App\Exception\NotFoundException;
use App\Repository\CategoryRepository;

class CategoryHelper
{
    /** @var CategoryRepository */
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getCategories(): array
    {
        $categories = $this->repository->findPublic();

        if (!empty($categories)) {
            return $categories;
        }

        throw new NotFoundException();
    }

    public function getCategoryBySlug($slug): array
    {
        $category = $this->repository->findOneBySlug($slug);

        if (!empty($category)) {
            return (array)$category;
        }

        throw new NotFoundException();
    }
}
