<?php

namespace App\Helper;

use App\Exception\NotFoundException;
use App\Repository\ProductRepository;

class ProductHelper
{
    const PER_PAGE_DEFAULT_VALUE = 30;

    /** @var ProductRepository */
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getProducts(int $categoryId, array $filters = []): array
    {
        $limit = $filters['limit'] ?? self::PER_PAGE_DEFAULT_VALUE;
        $offset = (($filters['page'] ?? 1) - 1) * $limit;

        $products = $this->repository->findByCateogry($categoryId, $limit, $offset);

        if (!empty($products)) {
            return $products;
        }

        throw new NotFoundException();
    }

    public function getProductBySlug($slug): array
    {
        $product = $this->repository->findOneBySlug($slug);

        if (!empty($product)) {
            return (array)$product;
        }

        throw new NotFoundException();
    }
}
