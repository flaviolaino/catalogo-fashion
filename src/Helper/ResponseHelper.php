<?php

namespace App\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseHelper
{
    public static function ok(array $data): JsonResponse
    {
        return new JsonResponse([
            'success' => true,
            'error' => null,
            'data' => $data
        ]);
    }

    public static function ko(string $error, int $errorCode = JsonResponse::HTTP_BAD_REQUEST): JsonResponse
    {
        return new JsonResponse([
            'success' => false,
            'error' => $error,
            'data' => []
        ], $errorCode);
    }
}