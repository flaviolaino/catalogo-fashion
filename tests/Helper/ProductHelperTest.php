<?php

namespace App\Tests\Helper;

use App\Client\HttpClient;
use App\Helper\ProductHelper;
use App\Exception\NotFoundException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductHelperTest extends WebTestCase
{
    const EXISTING_SLUG = 'scarpe-ginnastica';
    const NOT_EXISTING_SLUG = 'infradito';
    const CATEGORY_ID = 1;

    /** @var ProductHelper */
    private $helper;

    public function setUp()
    {
        self::bootKernel();
        $this->helper = static::$container->get(ProductHelper::class);
    }

    public function testGetProducts(): void
    {
        $this->assertInstanceOf(ProductHelper::class, $this->helper);

        $products = $this->helper->getProducts(self::CATEGORY_ID);
        $this->assertIsArray($products);

        $publicValues = array_column($products, 'public');

        $this->assertCount(count($products), $publicValues, 'There is one or more product that don\'t have "public" key');

        $this->assertNotContains(false, $publicValues, 'There is one or more product that have "public" = 0');

        $categoryValues = array_column($products, 'category_id');

        $this->assertContains(self::CATEGORY_ID, $categoryValues, 'There is one or more product that don\'t have "category_id" = ' . self::CATEGORY_ID);
    }

    public function testGetProductBySlug(): void
    {
        $existingProduct = $this->helper->getProductBySlug(self::EXISTING_SLUG);
        $this->assertIsArray($existingProduct);

        $this->expectException(NotFoundException::class);
        $notExistingProduct = $this->helper->getProductBySlug(self::NOT_EXISTING_SLUG);
    }
}
