<?php

namespace App\Tests\Helper;

use App\Client\HttpClient;
use App\Helper\CategoryHelper;
use App\Exception\NotFoundException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryHelperTest extends WebTestCase
{
    const EXISTING_SLUG = 'scarpe';
    const NOT_EXISTING_SLUG = 'parrucche';

    /** @var CategoryHelper */
    private $helper;

    public function setUp()
    {
        self::bootKernel();
        $this->helper = static::$container->get(CategoryHelper::class);
    }

    public function testGetCategories(): void
    {
        $this->assertInstanceOf(CategoryHelper::class, $this->helper);

        $categories = $this->helper->getCategories();
        $this->assertIsArray($categories);

        $this->assertObjectHasAttribute('public', $categories[0]);

        $this->assertEquals('1', $categories[0]->public);
    }

    public function testGetCategoryBySlug(): void
    {
        $existingCategory = $this->helper->getCategoryBySlug(self::EXISTING_SLUG);
        $this->assertIsArray($existingCategory);

        $this->expectException(NotFoundException::class);
        $notExistingCategory = $this->helper->getCategoryBySlug(self::NOT_EXISTING_SLUG);
    }
}
